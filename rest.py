# connect to the rest api of ARUBA AP
# ip are in devices_iap
import requests
import json
import prometheus_client
from prometheus_client.core import GaugeMetricFamily, REGISTRY
from prometheus_client import MetricsHandler
from http.server import HTTPServer
import time
import urllib3
import os
# from dotenv import load_dotenv

# load_dotenv()

urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

# read username and password env variable
username = os.environ['ARUBA_USERNAME']
password = os.environ['ARUBA_PASSWORD']
ip = os.environ['ARUBA_IP']
# ip = "192.168.0.199"


class HTTPRequestHandler(MetricsHandler):
    def do_GET(self):
        if self.path == '/metrics':
            return super().do_GET()
        else:
            self.send_response(404)
            self.end_headers()


class ArubaCollector(object):
    def __init__(self):
        pass

    def collect(self):
        # ip = "192.168.0.201"
        # ip = "setmeup.arubanetworks.com"
        # ip =
        # username = 'admin'
        # password = "haply"
        # get the json auth token
        url = "https://" + ip + ":4343/rest/login"
        json_data = {"user": username, "passwd": password}
        payload = json.dumps(json_data)

        # request as Accept : Application/json
        headers = {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        }
        response = requests.request(
            "POST", url, data=payload, verify=False, headers=headers)
        # get the response status and print it
        if "200" in str(response.status_code):
            sid = response.json()['sid']
            print("Login Success")
        else:
            print("Login Failed")
            print(response.status_code)
            print(response.text)

        # url for show command
        url = "https://" + ip + ":4343/rest/show-cmd?iap_ip_addr=" + \
            ip + "&cmd=show%20clients&sid=" + sid
        json_data = {"cmd": "show clients", "sid": sid}
        payload = json.dumps(json_data)
        response = requests.request(
            "GET", url, data=payload, verify=False, headers=headers)
        # get the response status and print it
        clients = []
        if "200" in str(response.status_code):
            print("prcessing")
            output = response.json()['Command output']
            #  remove the first 8 lines of the output
            output = output.splitlines()
            output = output[8:]
            for line in output:
                # split by space
                # if first caracter is null replace by X
                if line[0] == " ":
                    line = line.replace(" ", "X", 1)
                # split by tab
                line = [s.strip() for s in line.split('  ') if s]
                # print length
                if len(line) > 10:
                    if len(line) == 12:
                        line[10] = line[10].split("(")[0]
                        line[11] = line[11].split("(")[0]
                        if line[0] == "X":
                            line[0] = line[1]
                        json_line = {
                            "Name": line[0],
                            "IP": line[1],
                            "MAC": line[2],
                            "OS": line[3],
                            "ESSID": line[4],
                            "AP": line[5],
                            "Channel": line[6],
                            "Type": line[7],
                            "BSSID": line[8],
                            "Signal": line[10],
                            "Speed": line[11]
                        }
                    else:
                        json_line = {
                            "Name": line[0],
                            "IP": line[1],
                            "MAC": line[2],
                            "OS": "Unknown",
                            "ESSID": line[3],
                            "AP": line[4],
                            "Channel": line[5],
                            "Type": line[6],
                            "BSSID": line[7],
                            "Signal": line[9],
                            "Speed": line[10]
                        }
                    clients.append(json_line)

                    # print(line[0])
            # generate prometheus format
            test = GaugeMetricFamily("test", "test")
            print("generating prometheus format")
            client_name = GaugeMetricFamily(
                "client_name", "Name of the client", labels=["ap", "OS", "BSSID", "Channel", "name"])
            client_Signal = GaugeMetricFamily(
                "client_Signal", "Level of Signal", labels=["ap", "OS", "BSSID", "Channel", "name"])
            client_Speed = GaugeMetricFamily(
                "client_Speed", "Speed of Connection", labels=["ap", "OS", "BSSID", "Channel", "name"])
            for client in clients:
                # check that value is not null and a float
                client_name.add_metric(labels=[
                                       client['AP'], client['OS'], client['BSSID'], client['Channel'], client["Name"]], value=1.0)
                # client_name.labels(
                #    client['AP'], client['OS'], client['BSSID'], client['Channel'], client["Name"]).set(1.0)
                # keep only the number and scrap the letters after
                client["Signal"] = client["Signal"].split("(")[0]
                client["Speed"] = client["Speed"].split("(")[0]
                client_Signal.add_metric(
                    [client['AP'], client['OS'], client['BSSID'], client['Channel'], client["Name"]], client["Signal"])
                client_Speed.add_metric(
                    [client['AP'], client['OS'], client['BSSID'], client['Channel'], client["Name"]], client["Speed"])
            yield client_name
            yield client_Signal
            yield client_Speed


if __name__ == "__main__":
    port = int(os.environ['PROMETHEUS_PORT'])
    server = ('0.0.0.0', port)
    print(port)

    # prometheus_client.start_http_server(port)
    prometheus_client.REGISTRY.register(ArubaCollector())
    # REGISTRY.register(RandomNumberCollector())
    HTTPServer(server, HTTPRequestHandler).serve_forever()

# make a simple server to serve the prometheus data on port 8000 \metrics
