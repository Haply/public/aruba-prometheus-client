from python:3.11
RUN pip install --upgrade pip
RUN python -m pip install prometheus_client requests

COPY rest.py /root

# define ENV variables
ENV PROMETHEUS_PORT 9090
ENV ARUBA_USERNAME admin
ENV ARUBA_IP 192.168.0.199
ENV ARUBA_PASSWORD password

EXPOSE 9090
ENTRYPOINT ["python", "/root/rest.py"]
